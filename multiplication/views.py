from rest_framework.decorators import api_view
from rest_framework.response import Response
from multiplication.task import mul

# Create your views here.

@api_view(['GET', 'POST'])
def multiplication(request):
    if request.method == 'POST':
        mul(request.data["num_1"], request.data["num_2"])
        return Response({"message": "mul published task"})
    return Response({ "num_1": 1, "num_2": 2 })