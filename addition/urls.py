
from django.urls import path
from addition.views import addition


urlpatterns = [
    path('addition', addition, name='addition'),
]