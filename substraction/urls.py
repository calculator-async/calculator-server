
from django.urls import path
from substraction.views import substraction


urlpatterns = [
    path('substraction', substraction, name='substract'),
]