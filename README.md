
# [Run RabbitMQ with Docker](https://hub.docker.com/_/rabbitmq)

docker run -d --hostname calculator-rabbit --name calculator-rabbit -p 5672:5672 -p 15672:15672 -e RABBITMQ_DEFAULT_USER=root -e RABBITMQ_DEFAULT_PASS=root rabbitmq:3-management

# [Configure the host in celery](https://docs.celeryq.dev/en/latest/django/first-steps-with-django.html#using-celery-with-django)

ensure that celery is correctly configured in calculator/settings.py -> CELERY_BROKER_URL

# Run celery

celery -A calculator worker -l INFO -P threads

# Run Monitoring

celery -A calculator flower